package cloud.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProdutoDTO {
	
	 Long idProduto;
	 String descricao;
	 String descricaoCompleta;
	 Double precoCusto;
	 Double precoVenda;
	 Double precoComposicao;
	 Long estoque;
	 Long idComposicao;
	 String imagem;

	
}
