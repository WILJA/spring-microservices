package cloud.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClienteDTO {
	
	 Long idCliente;
	 Long cpf;
	 String nome;
	 String email;
	 Long idEndereco;
	
}
