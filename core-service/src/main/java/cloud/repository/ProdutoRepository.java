package cloud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import cloud.model.Produto;

@RepositoryRestResource(path = "/produtos", collectionResourceRel = "produtos")
public interface ProdutoRepository extends JpaRepository<Produto, Long>{
	
}