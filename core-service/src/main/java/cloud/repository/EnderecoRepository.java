package cloud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import cloud.model.Endereco;

@RepositoryRestResource(path = "/enderecos", collectionResourceRel = "enderecos")
public interface EnderecoRepository extends JpaRepository<Endereco, Long>{
	
}