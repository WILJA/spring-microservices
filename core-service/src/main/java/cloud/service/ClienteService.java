package cloud.service;

import org.springframework.stereotype.Service;

import cloud.model.Cliente;
import cloud.repository.ClienteRepository;
import cloud.support.AbstractService;


@Service
public class ClienteService extends AbstractService<Cliente, ClienteRepository> {
}
