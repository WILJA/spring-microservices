package cloud.service;

import org.springframework.stereotype.Service;

import cloud.model.Endereco;
import cloud.repository.EnderecoRepository;
import cloud.support.AbstractService;


@Service
public class EnderecoService extends AbstractService<Endereco, EnderecoRepository> {
}
