package cloud.service;

import org.springframework.stereotype.Service;

import cloud.model.Usuario;
import cloud.repository.UsuarioRepository;
import cloud.support.AbstractService;


@Service
public class UsuarioService extends AbstractService<Usuario, UsuarioRepository> {
}
