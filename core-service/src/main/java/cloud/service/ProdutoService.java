package cloud.service;

import org.springframework.stereotype.Service;


import cloud.model.Produto;
import cloud.repository.ProdutoRepository;
import cloud.support.AbstractService;


@Service
public class ProdutoService extends AbstractService<Produto, ProdutoRepository> {
}
