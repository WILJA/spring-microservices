package cloud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import cloud.support.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Endereco extends BaseEntity {

	@Id @GeneratedValue
	 Long idEndereco;
	 String logradouro;
	 Long numero;
	 Long cep;
	 String bairro;
	 String cidade;
	 String uf;

}