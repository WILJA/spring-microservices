package cloud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import cloud.support.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Produto extends BaseEntity {

	@Id @GeneratedValue
	 Long idProduto;
	 String descricao;
	 String descricaoCompleta;
	 Double precoCusto;
	 Double precoVenda;
	 Double precoComposicao;
	 Long estoque;
	 Long idComposicao;
	 String imagem;

}