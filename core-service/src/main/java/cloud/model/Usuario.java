package cloud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import cloud.support.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Usuario extends BaseEntity {

	@Id @GeneratedValue
	Long idUsuario;
	String login;
	String senha;
	Long idCliente;
	Long grupo;

}