package core.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cloud.core.DisciplinaServiceProxy;
import cloud.dto.ProdutoDTO;
import cloud.model.Produto;
import cloud.repository.ProdutoRepository;
import cloud.service.ProdutoService;
import cloud.support.AbstractController;



@RestController
@RequestMapping("/produtos")
public class ProdutoRestController  extends AbstractController<Produto, ProdutoService> {
	
    protected static Logger log = LoggerFactory.getLogger(ProdutoRestController.class);
	
	@Autowired
	ProdutoRepository repository;
	
	@Autowired
	DisciplinaServiceProxy disciplinaProxy;
	
	@PreAuthorize("#oauth2.isUser()")
	@GetMapping("/nomes")
	public List<String> getProdutos() {
		return repository.findAll()
				.stream().map(a -> a.getDescricao()).collect(Collectors.toList());
	}

	@GetMapping("/{id}/dto")
	@SuppressWarnings("all")
	public ProdutoDTO getProduto(@PathVariable Long id) throws Exception {

		//List<String> nomesDisciplinas = disciplinaProxy.getNomesDisciplinas();
				
		Produto Produto = repository.findOne(id);

		return ProdutoDTO.builder().idProduto(Produto.getIdProduto())
				.idProduto(Produto.getIdProduto())
				.descricao(Produto.getDescricao())
				.descricaoCompleta(Produto.getDescricaoCompleta())
				.precoCusto(Produto.getPrecoCusto())
				.precoVenda(Produto.getPrecoVenda())
				.precoComposicao(Produto.getPrecoComposicao())
				.estoque(Produto.getEstoque())
				.idComposicao(Produto.getIdComposicao())
				.imagem(Produto.getImagem())
				.build();

				

	}

	
}