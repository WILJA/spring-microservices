package core.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cloud.core.DisciplinaServiceProxy;
import cloud.dto.EnderecoDTO;
import cloud.model.Endereco;
import cloud.repository.EnderecoRepository;
import cloud.service.EnderecoService;
import cloud.support.AbstractController;



@RestController
@RequestMapping("/enderecos")
public class EnderecoRestController  extends AbstractController<Endereco, EnderecoService> {
	
    protected static Logger log = LoggerFactory.getLogger(EnderecoRestController.class);
	
	@Autowired
	EnderecoRepository repository;
	
	@Autowired
	DisciplinaServiceProxy disciplinaProxy;
	
	@PreAuthorize("#oauth2.isUser()")
	@GetMapping("/nomes")
	public List<String> getEnderecos() {
		return repository.findAll()
				.stream().map(a -> a.getLogradouro()).collect(Collectors.toList());
	}

	@GetMapping("/{id}/dto")
	@SuppressWarnings("all")
	public EnderecoDTO getEndereco(@PathVariable Long id) throws Exception {

		//List<String> nomesDisciplinas = disciplinaProxy.getNomesDisciplinas();
				
		Endereco endereco = repository.findOne(id);

		return EnderecoDTO.builder().idEndereco(endereco.getIdEndereco())
				.logradouro(endereco.getLogradouro())
				.numero(endereco.getNumero())
				.bairro(endereco.getBairro())
				.cep(endereco.getCep())
				.cidade(endereco.getCidade())
				.uf(endereco.getUf())
				.build();
	}

	
}